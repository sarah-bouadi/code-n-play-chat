import * as http from 'http';
import {Server, Socket} from 'socket.io';

import express from "express";
import cors from "cors";

const app = express();
app.use(cors());

const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    }
});

interface Message {
    idRoomChat: number;
    sender: string;
    content: string;
}

io.on('connection', (socket: Socket) => {
    console.log('a user connected');

    socket.on('joinRoom', (idRoomChat: number) => {
        if (idRoomChat){
            const roomId = idRoomChat.toString();

            // Check if socket is already in the room
            if (!socket.rooms.has(roomId)) {
                socket.join(roomId);
                console.log(`User joined room: ${idRoomChat}`);
            } else {
                console.log(`User already in room: ${idRoomChat}`);
            }
        }
    });

    socket.on('chat message', (msg: Message) => {
        const {idRoomChat, sender, content} = msg;
        console.log(`Received message in room ${idRoomChat} from ${sender}: ${content}`);

        if (idRoomChat){
            if (io.sockets.adapter.rooms.get(idRoomChat.toString())) {
                console.log(`Emitting message to room ${idRoomChat}:`, msg);
                io.to(idRoomChat.toString()).emit('chat message', msg);
            } else {
                console.log(`No users in room ${idRoomChat}. Message not sent.`);
            }
        }
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

server.listen(3999, () => {
    console.log(`listening on *:3999`);
});
