FROM --platform=linux/amd64 node:18

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm install -g typescript

RUN tsc

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

EXPOSE 3999

CMD [ "node", "index.js" ]
